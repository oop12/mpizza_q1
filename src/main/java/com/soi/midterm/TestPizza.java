/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.midterm;

/**
 *
 * @author user
 */
public class TestPizza {

    public static void main(String[] args) {
        PizzaHouse();
        BbqChicken pizza1 = new BbqChicken("Jane", "BBQ Chicken", 's');
        pizza1.printStatus();
        pizza1.total();

        PizzaHouse();
        BaconCheese pizza2 = new BaconCheese("Sara", "Bacon Cheese", 'm');
        pizza2.printStatus();
        pizza2.total();

        PizzaHouse();
        Sausage pizza3 = new Sausage("Jack", "Sausage", 'L');
        pizza3.printStatus();
        pizza3.total();

        PizzaHouse();
        BaconCheese pizza4 = new BaconCheese("John", "Bacon Cheese", 'L');
        pizza4.printStatus();
        pizza4.total();

        PizzaHouse();
        BbqChicken pizza5 = new BbqChicken("Bella", "BBQ Chicken", 'L');
        pizza5.printStatus();
        pizza5.total();
    }

    private static void PizzaHouse() {
        System.out.println("-----PIZZA HOUSE-----");
    }
}
