/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.midterm;

/**
 *
 * @author user
 */
public class Sausage {

    private String name;
    private String topping;
    private char size;
    private int s = 79;
    private int m = 99;
    private int l = 109;

    public Sausage(String name, String topping, char size) {
        this.name = name;
        this.topping = topping;
        this.size = size;
    }

    public int price(char size) {
        int price = 0;
        switch (size) {
            case 'S':
            case 's':
                price = s;
                break;
            case 'M':
            case 'm':
                price = m;

                break;
            case 'L':
            case 'l':
                price = l;

                break;
            default:
                price = 0;
                break;
        }
        return price;

    }

    public void printStatus() {
        System.out.println("Your order : " + this.name + "\n Topping : " + this.topping
                + " \n Size : " + this.size + " = " + price(size) + " Bath");
    }

    public void total() {
        int total = price(size);
        System.out.println("Total : " + total + " Bath");

    }

}
